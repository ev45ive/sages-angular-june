$(document).ready(function(){
	
	var items = $('.invoice-item');
	var total = $('.output-total');
	
	function CalculateTotal(){
		return items
		.find('.output-subtotal')
		.toArray()
		.reduce(SumReducer,0)
	}

	items.each(function(i, item){
		var $item = $(item),
			price = $item.find('.input-price'),
			count = $item.find('.input-count'),
			subtotal = $item.find('.output-subtotal');

		price.on('change keyup', function(event){		
			subtotal.text( price.val() * count.val() );
			total.text(CalculateTotal());

		})
		count.on('change keyup', function(event){
			subtotal.text( price.val() * count.val() );	
			total.text(CalculateTotal());
		})
	})


	function SumReducer(aggregate,currentItem){
		return aggregate + parseInt(currentItem.innerHTML,10);
	}

})